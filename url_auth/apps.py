from django.apps import AppConfig


class UrlAuthConfig(AppConfig):
    name = 'url_auth'
