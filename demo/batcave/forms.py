from datetime import timedelta
from django import forms
from django.contrib.auth import get_user_model

from url_auth.utils import generate_login_url


USER = get_user_model()


class CreateLoginURLForm(forms.Form):
    user = forms.ModelChoiceField(queryset=USER.objects.all())
    redirect_url = forms.URLField()
    url_valid_till = forms.IntegerField()

    def get_login_url(self):
        data = self.cleaned_data
        return generate_login_url(
                data['user'],
                redirect_url=data['redirect_url'],
                expiry_time_delta=timedelta(minutes=data['url_valid_till'])
        )
