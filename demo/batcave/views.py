from django.views.generic.edit import FormView
from django.shortcuts import render
from batcave.forms import CreateLoginURLForm
from django.contrib.auth.mixins import LoginRequiredMixin


class GenerateLoginURL(LoginRequiredMixin, FormView):
    login_url = '/admin/login/'
    template_name = 'batcave/create_url.html'
    form_class = CreateLoginURLForm
    success_url = '/'

    def form_valid(self, form):
        login_url = form.get_login_url()
        scheme = self.request.is_secure() and "https" or "http"
        context = {
            'url_auth_login_url': '{}://{}{}'.format(
                scheme,
                self.request.META['HTTP_HOST'],
                login_url)
        }
        return render(self.request, 'batcave/display_url.html', context)
