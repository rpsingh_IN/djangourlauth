from django.apps import AppConfig


class BatcaveConfig(AppConfig):
    name = 'batcave'
